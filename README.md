CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers

INTRODUCTION
------------

This module sent your Drupal logs to [Grafana Loki.](https://grafana.com/docs/loki/latest/)

The Loki credentials and labels are configurable in the UI and in the
settings.php file.

After reading this you are able to sent Drupal logs to the remote Loki server.

* *This module is the bridge between the Drupal logs and the remote
Loki server*.
* This module is an extension of the
  [Drupal 'Monolog' module.](https://www.drupal.org/project/monolog)
* Thanks to this extension, this module has an integration with the
  ['Monolog - Logging for PHP' library by Seldaek.](https://github.com/Seldaek/monolog)
* This module has an additional integration with
  ['Monolog loki' library by itspire.](https://github.com/itspire/monolog-loki)

Some benefits of using this module are:

* Configurable Loki endpoint (URL of the remote Loki server).
* Configurable Loki labels.
* Configurable Fallback option, this is the recommended usage.
* Possibilities to add other log handler in combination with the loki handler.

Thanks to the power and flexibility of Monolog.

### 1. About Grafana Loki

[Grafana Loki](https://grafana.com/docs/loki/latest/) is a log aggregation tool,
and it is the core of a fully-featured logging stack.

Loki is a datastore optimized for efficiently holding log data.
The efficient indexing of log data distinguishes Loki from other logging
systems.
Unlike other logging systems, a Loki index is built from labels, leaving the
original log message unindexed.

### 2. About the Drupal 'Monolog' module

In the [README file](https://git.drupalcode.org/project/monolog/-/blob/2.x/README.md)
of the *Drupal 'Monolog' module*, you will find more information about:
* The integration of 'Monolog - Logging for PHP' library.
* The benefits of the Drupal 'Mondolog' module.
  * How to register ***handlers*** as services in the
    [Drupal Service Container](https://www.drupal.org/docs/8/api/services-and-dependency-injection/services-and-dependency-injection-in-drupal-8)
  * How to alter the format of the message using ***formatters***
    and how to register the formatters as services in the
    [Drupal Service Container](https://www.drupal.org/docs/8/api/services-and-dependency-injection/services-and-dependency-injection-in-drupal-8)
  * How to alter the message using ***processors***.

### 3. About the 'Monolog - Logging for PHP' library

For more information on how the *'Monolog - Logging for PHP' library*
itself works, take a look to the
[documentation](https://github.com/Seldaek/monolog/blob/master/doc/01-usage.md).

### 4. About the 'Monolog loki' library

For more information on how the *'Monolog loki' library* itself works,
take a look to
[README file](https://github.com/itspire/monolog-loki/blob/master/README.md).

CONFIGURATION
-------------

### 1. How it works

With this module you can setup the Loki configuration and sent the Drupal logs
to the remote Loki server.

1. In the `monolog_loki.services.yml`
   * register the ***'monolog'-handler(s)*** for Loki or other log handlers.
2. In the `settings.php`,
   * add the ***'monolog loki'-service(s)*** to Drupal container YAML.
   * add static or dynamic configuration variables.
3. UI configuration
   * add static configuration variables.

About the monolog configuration in the service files, take a look at the
[configuration section of the 'Monolog'-module](https://git.drupalcode.org/project/monolog#configuration)
for more information.

### 2. Recommended usage

Since Loki log handling uses a remote server.
This logging is prone to be subject of timeout, network shortage and so on.
To avoid your application being broken in such a case, we recommend wrapping
the handler in a ***WhatFailureGroupHandler***.

### 3. Example

In this example, the Drupal logs are sent to the remote ***Loki server*** and
***syslog*** as ***DEFAULT***.

**REMARK**: The syslog configuration is not the purpose of this README.
We won't go into more detail here and specific adjustments are expected to
improve this syslog configuration.

#### 3.1. Create the `monolog_loki.services.yml` file

The *'monolog'-service* are located in `monolog.services.yml` file.

Because of [recommended usage](#42-recommended-usage),
create a ***'monolog loki'-service*** that contains the recommended
***WhatFailureGroupHandler*** wrapper.
In this *'monolog loki'-service*, the ***'monolog'-handler(s)*** for Loki and
syslog are registered.

STEP 1: Create a `monolog_loki.services.yml` file in the `sites/default`
directory of the Drupal root.

```
parameters:
  monolog.channel_handlers:
    default: ['loki_with_fallback']

services:
  monolog.handler.loki_with_fallback:
    class: Monolog\Handler\WhatFailureGroupHandler
    arguments: [ [ '@monolog.handler.loki', '@monolog.handler.syslog' ] ]
  monolog.handler.loki:
    class: Itspire\MonologLoki\Handler\LokiHandler
    arguments:
      - entrypoint: 'http://localhost:3100'
        client_name: '<client-name>'
        labels:
          <label-1>: '<value-label-1>'
          <label-2>: '<value-label-2>'
  monolog.handler.syslog:
    class: Monolog\Handler\SyslogHandler
    arguments: ['myfacility', 'local6']
```
##### Legend

* `<client-name>` is a placeholder for the client name of the Loki Server.
* `<label-1>` is a placeholder for one of the Loki labels.
* `<label-2>` is a placeholder for one of the Loki labels.
* `<value-label-1>` is a placeholder for value of the first Loki label.
* `<value-label-2>` is a placeholder for value of the second Loki label.

#### 3.2. Update 'settings.php' file

Add following code in the `settings.php`.

Get site directory. Default is `sites/default`.
```
$site_dir = $app_root . '/' . $site_path;
```

Add the site specific *'monolog loki'-service* to the Drupal container YAML.
```
$settings['container_yamls'][] = $site_dir . '/monolog_loki.services.yml';
```

#### 3.3. UI configuration

UI path: `/admin/config/development/monolog_loki`

On this admin page you're able
* to set the entrypoint of the Loki Server.
* to set the client name.
* to set the custom labels and their values.
* to set the labels of the log messages as a Loki Label.


#### 3.4. Static and dynamic configuration in 'settings.php' file

Set ***static variables***.

```
$config['monolog_loki.settings']['overridden']['entrypoint'] 
  = 'https://localhost:3100';
$config['monolog_loki.settings']['overridden']['client_name'] = '';
$config['monolog_loki.settings']['overridden']['labels'] = [
  [
    'key' => 'host',
    'value' => 'example.com',
  ],
  [
    'key' => 'server_name',
    'value' => 'example.com',
  ],
];
$config['monolog_loki.settings']['log_labels']['channel'] = TRUE;
$config['monolog_loki.settings']['log_labels']['level_name'] = TRUE;
```

Set a ***dynamic variable*** into the value of a Loki label.

```
$config['monolog_loki.settings']['overridden']['labels'] = [
  [
    'key' => 'host',
    'value' => $_ENV['HTTP_HOST'] ?? 'example.com',
  ],
  [
    'key' => 'server_name',
    'value' => getenv('SERVER_NAME') ?? 'example.com',
  ],
];
```

#### 3.5. CURL payload to Loki Server and Query on Loki Server

When you run the cron on the following page `admin/config/system/cron`.
And based on following config in `settings.php`.

```
$config['monolog_loki.settings']['overridden']['entrypoint'] 
  = 'https://localhost:3100';
$config['monolog_loki.settings']['overridden']['client_name'] = 'example.com';
$config['monolog_loki.settings']['overridden']['labels'] = [
  [
    'key' => 'job',
    'value' => 'drupal',
  ],
];
$config['monolog_loki.settings']['log_labels']['channel'] = TRUE;
$config['monolog_loki.settings']['log_labels']['level_name'] = TRUE;
$config['monolog_loki.settings']['log_labels']['uid'] = TRUE;
```

The CURL payload is:
```
curl -v -H "Content-Type: application/json" \
-XPOST -s "https://loki.core.a51.be/loki/api/v1/push" --data-raw \
'
{
  "streams": [
    {
      "stream": {
        "job": "drupal",
        "host": "example.com",
        "channel": "cron",
        "level_name": "INFO",
        "uid": "1"
      },
      "values": [
        [
          "1648463911000000000",
          "{\"message\":\"Cron run completed.\",
          \"level\":200,
          \"level_name\":\"INFO\",
          \"channel\":\"cron\",
          \"datetime\":\"2022-03-28T22:22:22+01:00\",
          \"referer\":\"https://example.com/admin/config/system/cron\",
          \"ip\":\"::1\",
          \"request_uri\":\"https://example.com/admin/config/system/cron\",
          \"uid\":\"1\",\"user\":\"admin\"}"
        ]
      ]
    }
  ]
}
'
```

The lookup query on Loki server:
```
https://localhost:3100/loki/api/v1/query_range?query={job="drupal",channel="cron"}
```

FAQ
---

Q: **I didn't added the "host"-label, but this label is sent to the Loki Server.
Is this normal?**

A: Yes, in the the ['Monolog loki' library by itspire](https://github.com/itspire/monolog-loki)
, the "client_name" is copied to the "host"-label.

Q: **Is the "host"-label overridable?**

A: Yes it's possible.
* See the example in the ["3.4. Static and dynamic configuration in 'settings.php' file"-section](#34-static-and-dynamic-configuration-in-settingsphp-file)

```
$config['monolog_loki.settings']['labels'] = [
  [
    'key' => 'host',
    'value' => $_ENV['HTTP_HOST'] ?? 'example.com',
  ]
];
```

Q: **what is "client_name"-field used for? And why is this field not required?**

A: In the the ['Monolog loki' library by itspire](https://github.com/itspire/monolog-loki)
, the default value of the "client_name"-field is
[gethostname()](https://www.php.net/manual/en/function.gethostname.php).
If the value of the "host"-label is empty then the "client_name" is copied
to the "host"-label.

Q: **Is it possible to add more than 5 Loki labels?**

A: yes it's possible.
* See the example in this section.
  ["3.4. Static and dynamic configuration in 'settings.php' file"-section](#34-static-and-dynamic-configuration-in-settingsphp-file)
* Remark: Be careful when using too much label.
  [See Grafana Loki documentation](https://grafana.com/docs/loki/latest/fundamentals/labels/#optimal-loki-performance-with-parallelization)

Q: **Is it possible to add more than 2 log-labels as Loki label?**

A: yes it's possible.
This example shows you how to add the 'uid'-log-label as a Loki Label.
* See the example in this section.
  ["3.4. Static and dynamic configuration in 'settings.php' file"-section](#34-static-and-dynamic-configuration-in-settingsphp-file)

```
$config['monolog_loki.settings']['log_labels']['uid'] = TRUE;
```

Supporting organizations:
* VRT - https://www.drupal.org/node/2859102
