<?php

namespace Drupal\monolog_loki\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Monolog Loki Settings form.
 */
class MonologLokiSettingsForm extends ConfigFormBase {

  const LABELS_TOTAL = 5;
  const LABELS_TOTAL_START_POSITION = 1;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'monolog_loki_settings_form';
  }

  /**
   * {@inheritdoc}
   *
   * Why? Return [] and not ['monolog_loki.settings'].
   * Because we want the settings to override the configuration in settings.php.
   */
  protected function getEditableConfigNames() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    if (\Drupal::moduleHandler()->moduleExists('help')) {
      $form['overriden']['@help-page'] = Url::fromRoute('help.page', ['name' => 'monolog_loki'])->toString();
      $form['log_labels']['@help-page'] = Url::fromRoute('help.page', ['name' => 'monolog_loki'])->toString();
    }
    $config = $this->config('monolog_loki.settings');
    $form['overridden'] = [
      '#type' => 'details',
      '#title' => $this->t('Overridden service configuration'),
      '#description' => $this->t('Overrides the configuration from the "<em>monolog.handler.loki</em>"-service in the "<em>monolog_loki services yaml</em>"-file. For more information take a look at the <a href="@help-page" target="_blank" title="external link to readme file">Example section</a> in the README file.'),
      '#open' => TRUE,
    ];

    $form['overridden']['overridden_entrypoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Entrypoint'),
      '#maxlength' => 255,
      '#default_value' => $config->get('overridden.entrypoint'),
    ];

    $form['overridden']['overridden_client_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client name'),
      '#maxlength' => 255,
      '#default_value' => $config->get('overridden.client_name'),
    ];

    $form['overridden']['overridden_labels'] = [
      '#type' => 'details',
      '#title' => $this->t('Loki labels'),
      '#open' => TRUE,
    ];

    $labelsCounter = $this::LABELS_TOTAL_START_POSITION;
    $configLabels = (array) $config->get('overridden.labels');
    while ($labelsCounter <= $this::LABELS_TOTAL) {
      $formLabels = $this->getArrayFormLabels($labelsCounter);

      $form['overridden']['overridden_labels'][$formLabels['detail_label']['machine_name']] = [
        '#type' => 'details',
        '#title' => $this->t('%title', ['%title' => $formLabels['detail_label']['title']]),
        '#open' => TRUE,
      ];

      $form['overridden']['overridden_labels'][$formLabels['detail_label']['machine_name']][$formLabels['textfield_key']['machine_name']] = [
        '#type' => 'textfield',
        '#title' => $this->t('%title', ['%title' => $formLabels['textfield_key']['title']]),
        '#maxlength' => 55,
        '#default_value' => $configLabels[$labelsCounter - 1]['key'] ?? '',
      ];

      $form['overridden']['overridden_labels'][$formLabels['detail_label']['machine_name']][$formLabels['textfield_value']['machine_name']] = [
        '#type' => 'textfield',
        '#title' => $this->t('%title', ['%title' => $formLabels['textfield_value']['title']]),
        '#maxlength' => 255,
        '#default_value' => $configLabels[$labelsCounter - 1]['value'] ?? '',
      ];

      $labelsCounter++;
    }

    $form['log_labels'] = [
      '#type' => 'details',
      '#title' => $this->t('log labels'),
      '#description' => $this->t('Which of the following Drupal log labels would you like to add as Loki Labels. For more information take a look at the <a href="@help-page" target="_blank" title="external link to readme file">Example section</a> in the README file.', []),
      '#open' => TRUE,
    ];

    $form['log_labels']['log_label_channel'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Channel'),
      '#default_value' => $config->get('log_labels.channel'),
    ];

    $form['log_labels']['log_label_level_name'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Level name'),
      '#default_value' => $config->get('log_labels.level_name'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Add settings to config.
    $config = $this->configFactory()
      ->getEditable('monolog_loki.settings');

    // Set config.
    $config->set('overridden.entrypoint', $form_state->getValue('overridden_entrypoint'));
    $config->set('overridden.client_name', $form_state->getValue('overridden_client_name'));
    $config->set('log_labels.channel', (bool) $form_state->getValue('log_label_channel'));
    $config->set('log_labels.level_name', (bool) $form_state->getValue('log_label_level_name'));
    // Set config labels.
    $labelsCounter = $this::LABELS_TOTAL_START_POSITION;
    $labelsArray = [];
    while ($labelsCounter <= $this::LABELS_TOTAL) {
      $formLabels = $this->getArrayFormLabels($labelsCounter);
      $labelsArray[] = [
        'key' => $form_state->getValue($formLabels['textfield_key']['machine_name']),
        'value' => $form_state->getValue($formLabels['textfield_value']['machine_name']),
      ];
      $labelsCounter++;
    }
    $config->set('overridden.labels', $labelsArray);

    // Save config.
    $config->save();

    // Invalidate cache tag.
    Cache::invalidateTags([
      'monolog_loki:settings',
      'config:monolog_loki.settings',
    ]);

    parent::submitForm($form, $form_state);
  }

  /**
   * Get array of the form labels.
   *
   * @param \Drupal\monolog_loki\Form\int $labels_counter
   *   The counter of labels.
   *
   * @return \string[][]
   *   Returns a array list of labels.
   */
  protected function getArrayFormLabels(int $labels_counter): array {
    return [
      'detail_label' => [
        'machine_name' => 'overridden_label' . $labels_counter,
        'title' => 'Label ' . $labels_counter,
      ],
      'textfield_key' => [
        'machine_name' => 'overridden_label' . $labels_counter . '_key',
        'title' => 'Key ' . $labels_counter,
      ],
      'textfield_value' => [
        'machine_name' => 'overridden_label' . $labels_counter . '_value',
        'title' => 'Value ' . $labels_counter,
      ],
    ];
  }

}
