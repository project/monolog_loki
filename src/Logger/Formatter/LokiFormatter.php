<?php

namespace Drupal\monolog_loki\Logger\Formatter;

use Drupal\monolog_loki\Utility\LokiSettingsUtil;
use Itspire\MonologLoki\Formatter\LokiFormatter as MonologLokiFormatter;
use Monolog\LogRecord;

/**
 * Formats log records.
 */
class LokiFormatter extends MonologLokiFormatter {

  /**
   * Utilities for retrieving Loki Settings.
   *
   * @var \Drupal\monolog_loki\Utility\LokiSettingsUtil
   */
  protected $lokiSettingsUtil;

  /**
   * Constructs a LokiFormatter instance.
   *
   * @param \Drupal\monolog_loki\Utility\LokiSettingsUtil $lokiSettingsUtil
   *   Utility class to retrieve Loki settings.
   * @param array $labels
   *   Labels that will be used for all messages (optional).
   * @param array $context
   *   Base context to be used for all messages (optional).
   * @param string|null $systemName
   *   The system name (optional).
   * @param string $extraPrefix
   *   A prefix for 'extra' fields from the Monolog record (optional).
   * @param string $contextPrefix
   *   A prefix for 'context' fields from the Monolog record (optional).
   */
  public function __construct(
    LokiSettingsUtil $lokiSettingsUtil,
    array $labels = [],
    array $context = [],
    ?string $systemName = NULL,
    string $extraPrefix = '',
    string $contextPrefix = 'ctxt_',
  ) {
    $this->lokiSettingsUtil = $lokiSettingsUtil;

    parent::__construct($labels, $context, $systemName, $extraPrefix, $contextPrefix);
  }

  /**
   * {@inheritdoc}
   */
  public function format(LogRecord $record): array {
    $record = $record->toArray();
    $customLabels = $record['context']['labels'] ?? [];
    unset($record['context']['labels']);
    $record['context'] = array_merge($this->context, $record['context']);
    $preparedRecord = $this->prepareRecord($record);
    /** @var \DateTimeInterface $datetime */
    $datetime = $record['datetime'];

    return [
      'stream' => array_merge($this->labels, $customLabels, $this->getOverriddenLogLabels($preparedRecord)),
      'values' => [
        [
          (string) ($datetime->getTimestamp() * 1000000000),
          $this->toJson($this->normalize($preparedRecord)),
        ],
      ],
    ];
  }

  /**
   * Get overridden log labels.
   *
   * @param array $record
   *   An array of labels.
   *
   * @return array
   *   Returns an array of labels.
   */
  private function getOverriddenLogLabels(array $record): array {
    // Override default labels.
    $keepAsLabels = [];

    $logLabels = $this->lokiSettingsUtil->getLogLabels();
    if (!empty($logLabels)) {
      foreach ($logLabels as $labelName => $labelValue) {
        if ($labelValue) {
          $keepAsLabels[] = $labelName;
        }
      }
    }

    return array_filter(
        $record,
        fn($key): bool => in_array($key, $keepAsLabels, TRUE),
        ARRAY_FILTER_USE_KEY
    );
  }

}
