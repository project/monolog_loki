<?php

namespace Drupal\monolog_loki\Logger\Handler;

use Itspire\MonologLoki\Handler\LokiHandler as MonologLokiHandler;
use Drupal\monolog_loki\Logger\Formatter\LokiFormatter;
use Drupal\monolog_loki\Utility\LokiSettingsUtil;
use Monolog\Formatter\FormatterInterface;

/**
 * Forwards logs to loki.
 */
class LokiHandler extends MonologLokiHandler {

  /**
   * Utilities for retrieving Loki Settings.
   *
   * @var \Drupal\monolog_loki\Utility\LokiSettingsUtil
   */
  protected $lokiSettingsUtil;

  /**
   * Constructs a LokiHandler instance.
   *
   * @param \Drupal\monolog_loki\Utility\LokiSettingsUtil $lokiSettingsUtil
   *   Utility class to retrieve Loki settings.
   * @param array $apiConfig
   *   An array of api config, used in Loki.
   * @param int $level
   *   The minimum logging level at which this handler will be triggered.
   *   This cannot be from the Level Enum because before php 8.2 it is not
   *   allowed to get a value from an enum in a function signature.
   * @param bool $bubble
   *   Whether the messages that are handled can bubble up the stack or not.
   */
  public function __construct(LokiSettingsUtil $lokiSettingsUtil, array $apiConfig, $level = 'debug', $bubble = TRUE) {
    $this->lokiSettingsUtil = $lokiSettingsUtil;

    $apiConfig = $this->overrideApiConfig($apiConfig);

    parent::__construct($apiConfig, $level, $bubble);
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultFormatter(): FormatterInterface {
    return new LokiFormatter($this->lokiSettingsUtil, $this->globalLabels, $this->globalContext, $this->systemName);
  }

  /**
   * Override api config.
   *
   * @param array $apiConfig
   *   The api config.
   *
   * @return array
   *   Returns an array of overridden api config.
   */
  protected function overrideApiConfig(array $apiConfig = []): array {
    $newEntrypoint = $this->lokiSettingsUtil->getOverriddenEntrypoint();
    $newClientName = $this->lokiSettingsUtil->getOverriddenClientName();
    $newLabels = $this->lokiSettingsUtil->getOverriddenLabels(TRUE);

    if (!empty($newEntrypoint)) {
      $apiConfig['entrypoint'] = $newEntrypoint;
    }

    if (!empty($newClientName)) {
      $apiConfig['client_name'] = $newClientName;
    }

    if (!empty($newLabels)) {
      foreach ($newLabels as $key => $value) {
        if (!empty($key) && !empty($value)) {
          $apiConfig['labels'][$key] = $value;
        }
      }
    }

    return $apiConfig;
  }

}
