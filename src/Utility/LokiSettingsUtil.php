<?php

namespace Drupal\monolog_loki\Utility;

use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Provides a utility class with Loki setting helpers.
 */
class LokiSettingsUtil {

  /**
   * Loki settings config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $lokiSettings;

  /**
   * Constructs a LokiSettingsUtil instance.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Drupal's Config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->lokiSettings = $config_factory->get('monolog_loki.settings');
  }

  /**
   * Get Loki config settings object.
   *
   * @return \Drupal\Core\Config\ImmutableConfig
   *   Returns Loki config settings object.
   */
  public function getLokiSettings(): ImmutableConfig {
    return $this->lokiSettings;
  }

  /**
   * Get All settings of the Loki config.
   *
   * @return array
   *   Returns an array of all settings.
   */
  public function getSettings(): array {
    return $this->getLokiSettings()->get();
  }

  /**
   * Get the overridden Entrypoint of the Loki Server.
   *
   * @return string
   *   Returns the overridden entrypoint of the Loki server.
   */
  public function getOverriddenEntrypoint(): string {
    return (string) $this->getLokiSettings()->get('overridden.entrypoint');
  }

  /**
   * Get the overridden Loki Client Name.
   *
   * @return string
   *   Returns the overridden Loki Client Name.
   */
  public function getOverriddenClientName(): string {
    return (string) $this->getLokiSettings()->get('overridden.client_name');
  }

  /**
   * Get the overridden Loki labels.
   *
   * @param bool $return_as_assoc_arr
   *   A boolean, indicates if the array of labels is an associative array.
   *
   * @return array
   *   Returns an array of overridden Loki labels.
   */
  public function getOverriddenLabels(bool $return_as_assoc_arr = TRUE): array {
    $labels = (array) $this->getLokiSettings()->get('overridden.labels');

    if ($return_as_assoc_arr) {
      return $this->transformLabelsToAssocArray($labels);
    }

    return $labels;
  }

  /**
   * Transform Loki labels to associative array.
   *
   * @param array $lokiSettingsLabels
   *   An array of Loki labels.
   *
   * @return array
   *   Returns an associative array of Loki labels.
   */
  protected function transformLabelsToAssocArray(array $lokiSettingsLabels = []) {
    $apiConfigLabels = [];

    if (!empty($lokiSettingsLabels)) {
      foreach ($lokiSettingsLabels as $value) {
        if (!empty($value['key']) && !empty($value['value'])) {
          $apiConfigLabels[$value['key']] = $value['value'];
        }
      }
    }

    return $apiConfigLabels;
  }

  /**
   * Get the log labels.
   *
   * @return array
   *   Returns an array of log labels.
   */
  public function getLogLabels(): array {
    return (array) $this->getLokiSettings()->get('log_labels');
  }

}
