<?php

declare(strict_types=1);

namespace Drupal\monolog_loki;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceModifierInterface;
use Drupal\monolog_loki\Logger\Handler\LokiHandler;
use Monolog\Level;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Overrides the `monolog.handler.loki` service.
 */
class MonologLokiServiceProvider implements ServiceModifierInterface {

  /**
   * {@inheritDoc}
   */
  public function alter(ContainerBuilder $container) {
    if ($container->has('monolog.handler.loki')) {
      $definition = $container->getDefinition('monolog.handler.loki');

      // Override default class.
      $definition->setClass(LokiHandler::class);

      $args = $definition->getArguments();

      $apiConfigArg = $args[0] ?? [];
      $levelArg = $args[1] ?? Level::Debug->toPsrLogLevel();
      $bubbleArg = $args[2] ?? TRUE;
      $lokiSettingsUtilArg = new Reference('monolog_loki.util.settings');

      if (empty($args)) {
        $definition->setArgument(0, $lokiSettingsUtilArg);
      }
      else {
        $definition->replaceArgument(0, $lokiSettingsUtilArg);
      }
      $definition->setArgument(1, $apiConfigArg);
      $definition->setArgument(2, $levelArg);
      $definition->setArgument(3, $bubbleArg);
    }
  }

}
