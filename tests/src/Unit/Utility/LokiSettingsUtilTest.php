<?php

namespace Drupal\Tests\monolog_loki\Unit\Utility;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\monolog_loki\Utility\LokiSettingsUtil;
use Drupal\Tests\UnitTestCase;

/**
 * Test the logic in the utility class of the Loki setting helpers.
 *
 * @coversDefaultClass \Drupal\monolog_loki\Utility\LokiSettingsUtil
 * @group monolog_loki
 */
class LokiSettingsUtilTest extends UnitTestCase {

  /**
   * Testing VrtPlatformFileUtil class.
   *
   * @var \Drupal\monolog_loki\Utility\LokiSettingsUtil
   */
  protected $lokiSettingsUtil;

  /**
   * Invoke method.
   *
   * To access protected methods.
   *
   * @param mixed $object
   *   The object of the class.
   * @param string $methodName
   *   The method name.
   * @param array $arguments
   *   The arguments of the method.
   *
   * @return mixed
   *   Returns the return value of the protected method.
   *
   * @throws \ReflectionException
   */
  public function invokeMethod(&$object, string $methodName, array $arguments = []) {
    $reflection = new \ReflectionClass(get_class($object));
    $method = $reflection->getMethod($methodName);
    $method->setAccessible(TRUE,);

    return $method->invokeArgs($object, $arguments);
  }

  /**
   * Load config.
   *
   * @param array|null $reveal_data
   *   The reveal data.
   */
  protected function loadConfig(?array $reveal_data): void {
    $labelsSettings = $this->prophesize(ImmutableConfig::class);
    $labelsSettings->get('overridden.labels')
      ->willReturn($reveal_data);

    $configFactory = $this->prophesize(ConfigFactory::class);
    $configFactory->get('monolog_loki.settings')
      ->willReturn($labelsSettings->reveal());

    $this->lokiSettingsUtil = new LokiSettingsUtil($configFactory->reveal());
  }

  /**
   * Test get overridden labels.
   *
   * @param array|null $reveal_data
   *   The reveal data.
   * @param array|null $expected_as_default_arr
   *   The expected array of labels as default saved in the settings.
   * @param array|null $expected_as_assoc_arr
   *   The expected array of labels as an associative array.
   *
   * @throws \ReflectionException
   *
   * @covers ::getOverriddenLabels
   * @dataProvider dataProviderOverriddenLabels
   */
  public function testGetOverriddenLabels(?array $reveal_data, ?array $expected_as_default_arr, ?array $expected_as_assoc_arr) {
    $this->loadConfig($reveal_data);

    // The expected overridden labels as default array, saved in the settings.
    $labels = $this->invokeMethod($this->lokiSettingsUtil, 'getOverriddenLabels', [FALSE]);
    $this->assertEquals($expected_as_default_arr, $labels);

    // The expected overridden labels as an associative array.
    $labels = $this->invokeMethod($this->lokiSettingsUtil, 'getOverriddenLabels', [TRUE]);
    $this->assertEquals($expected_as_assoc_arr, $labels);
  }

  /**
   * Data provider of the Overridden labels.
   *
   * @return array
   *   Array of data.
   */
  public function dataProviderOverriddenLabels(): array {
    return [
      [
        'reveal_data' => [
          [
            'key' => 'job',
            'value' => 'drupal',
          ],
        ],
        'expected_as_default_arr' => [
          [
            'key' => 'job',
            'value' => 'drupal',
          ],
        ],
        'expected_as_assoc_arr' => [
          'job' => 'drupal',
        ],
      ],
      [
        'reveal_data' => [
          [
            'key' => 'job',
            'value' => '',
          ],
        ],
        'expected_as_default_arr' => [
          [
            'key' => 'job',
            'value' => '',
          ],
        ],
        'expected_as_assoc_arr' => [],
      ],
      [
        'reveal_data' => [
          [
            'key' => '',
            'value' => 'drupal',
          ],
        ],
        'expected_as_default_arr' => [
          [
            'key' => '',
            'value' => 'drupal',
          ],
        ],
        'expected_as_assoc_arr' => [],
      ],
    ];
  }

}
